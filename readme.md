# To-Do App
A basic todo app that creates notes in React. You can just put your to-do list in there.

## Live Demo
- [Click Me](https://to-do.ml)

### Screen Shot
![To-Do](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/xceq148xnxn1rpi261n3.png)

## Items that will be added in the future :

- Authorisation
- Backend 
- Import/Export



